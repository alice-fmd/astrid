// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: Detector.h,v 1.3 2006-06-20 23:59:43 hehi Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    astrid/Detector.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Detector
*/
#ifndef Detector_h
#define Detector_h
#ifndef Simulation_Task
# include <simulation/Task.h>
#endif
#ifndef ROOT_TArrayI
# include <TArrayI.h>
#endif
class TVector2;
class TGeoVolume;
class TGeoMedium;
class TLorentzVector;


/** @brief Class that handles hits in the sensors 

    @image html event.png
 */
class Detector :  public Simulation::Task 
{
public:
  /** Constructor */
  Detector();
  /** Register active medium 
      @param option Not used */
  virtual void Initialize(Option_t* option="");
  /** Register active volume 
      @param option Not used */
  virtual void Register(Option_t* option="");
  /** Process hit in active medium and volume */
  virtual void Step();
  ClassDef(Detector,0)
protected:
  /** Add a hit to output array
      @param track   Track number
      @param module  Sensor number
      @param sector  Sector number
      @param strip   Strip number
      @param eloss   Energy loss 
      @param v       Position
      @param p       Momentum
      @param pdg     Particle ID code
      @param len     LEngth throug element */
  void AddHit(Int_t track, UShort_t module, UShort_t sector, 
	      UShort_t strip, Float_t eloss, 
	      const TLorentzVector& v, const TLorentzVector& p, 
	      Int_t pdg, Float_t len);
};
    
#endif
//
//  EOF
// 
