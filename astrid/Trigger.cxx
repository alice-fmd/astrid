//____________________________________________________________________ 
//  
// $Id: Trigger.cxx,v 1.2 2006-06-20 23:59:43 hehi Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    astrid/Trigger.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementaion file for Trigger
*/
#ifndef Trigger_h
# include "Trigger.h"
#endif
#ifndef Geometry_h
# include "Geometry.h"
#endif
#ifndef Simulation_Main
# include <simulation/Main.h>
#endif
#ifndef Simulation_Hit
# include <simulation/Hit.h>
#endif
#ifndef ROOT_TLorentzVector
# include <TLorentzVector.h>
#endif
#ifndef ROOT_TParticle
# include <TParticle.h>
#endif
#ifndef ROOT_TVector2
# include <TVector2.h>
#endif
#ifndef ROOT_TDatabasePDG
# include <TDatabasePDG.h>
#endif
#ifndef ROOT_TVirtualMC
# include <TVirtualMC.h>
#endif
#ifndef ROOT_TGeoVolume
# include <TGeoVolume.h>
#endif
#ifndef ROOT_TGeoMedium
# include <TGeoMedium.h>
#endif
#ifndef ROOT_TGeoManager
# include <TGeoManager.h>
#endif
#ifndef ROOT_TArrayI
# include <TArrayI.h>
#endif
#ifndef ROOT_TTree
# include <TTree.h>
#endif

//____________________________________________________________________
Trigger::Trigger()
  : Simulation::Task("Trigger", "FMD Trigger")
{
  CreateDefaultHitArray();
}

//____________________________________________________________________
void 
Trigger::Initialize(Option_t* option)
{
  Simulation::Task::Initialize(option);
  // Silicon 
  RegisterMedium(gGeoManager->GetMedium("Scintilator"));
}


//____________________________________________________________________
void
Trigger::Register(Option_t* option)
{
  TTree* tree = 0;
  if ((tree = GetBaseTree("Hits"))) fBranch = tree->Branch(GetName(), &fCache);
  RegisterVolume(gGeoManager->GetVolume("Slat"));
}

//____________________________________________________________________
void
Trigger::Step()
{
  TVirtualMC* mc = TVirtualMC::GetMC();
  Int_t copy;
  Int_t vol = mc->CurrentVolID(copy);
  if (!IsSensitive(vol)) return;

  CreateDefaultHit();
}

//____________________________________________________________________
//
// EOF
//

  
