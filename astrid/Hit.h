// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: Hit.h,v 1.1 2006-06-21 07:06:30 hehi Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    astrid/Hit.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Hit
*/
#ifndef Hit_h
#define Hit_h
#ifndef Simulation_Hit
# include <simulation/Hit.h>
#endif

/** @brief Hits in the FMD sensors
 */
class Hit : public Simulation::Hit
{
public:
  /** Default constructor */
  Hit() 
    : Simulation::Hit(), 
      fSensor(0), 
      fSector(0), 
      fStrip(0), 
      fLength(0)
  {}
  /** Constructor 
      @param sensor Sensor number
      @param sector Sector number
      @param strip  Strip number
      @param nTrack Track number
      @param v      Position 
      @param p      Momentum
      @param eloss  Energy loss
      @param pdg    Particle ID code 
      @param len    Length through active element.   */
  Hit(UShort_t              sensor, 
      UShort_t              sector, 
      UShort_t              strip, 
      Int_t                 nTrack, 
      const TLorentzVector& v, 
      const TLorentzVector& p, 
      Float_t               eloss, 
      Int_t                 pdg, 
      Float_t               len)
    : Simulation::Hit(nTrack, pdg, v, p, eloss), 
      fSensor(sensor), 
      fSector(sector), 
      fStrip(strip), 
      fLength(len)
  {}
  /** Add @a eloss to the total energy loss 
      @param eloss Energy loss to add 
  */
  void AddToEnergyLoss(Float_t eloss) { fEnergyLoss += eloss; }
  /** Add @a len to the total length through the active element
      @param len length through the active element to add 
  */
  void AddToLength(Float_t len) { fLength += len; }
  /** Get the total length through the active element
      @return Length through active element (in centimeters) */
  Float_t Length() const { return fLength; }
  /** @return Get sensor number */
  UShort_t Sensor() const { return fSensor; }
  /** @return Get sector number */
  UShort_t Sector() const { return fSector; }
  /** @return Get strip number */
  UShort_t Strip()  const { return fStrip; }
  /** @return Get the name */ 
  const char* GetTitle() const;
  /** Print to standard out 
      @param option Option string */
  void Print(Option_t* option="P") const;
protected:
  /** Sensor number */
  UShort_t fSensor;
  /** Sector number */
  UShort_t fSector;
  /** Strip number */
  UShort_t fStrip;
  /** Total length through active element */
  Float_t  fLength;
  ClassDef(Hit,1); // Hits in sensors 
};

#endif
//
// EOF
//

