// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: Geometry.h,v 1.3 2006-06-20 23:59:43 hehi Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    astrid/Geometry.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Geometry
*/
#ifndef Geometry_h
#define Geometry_h
#ifndef Framework_Task
# include <framework/Task.h>
#endif
#ifndef ROOT_TVector2
# include <TVector2.h>
#endif
#ifndef ROOT_TVector3
# include <TVector3.h>
#endif
class TGeoVolume;
class TGeoMedium;


/** @brief Class to make the geometry
    
    @image html setup.png
 */
class Geometry :  public Framework::Task 
{
public:
  /** Get the static instance - singleton
      @return The static instance, possibly newly allocated. */
  static Geometry* Instance();
  /** Initialize the geometry.  Declares needed tracking mediums,
      etc. 
      @param option Not used */
  virtual void Initialize(Option_t* option="");
  /** Build the geometry. 
      @param option Not used. */
  virtual void Register(Option_t* option="");
  /** Called at the beginning of each event.  On first call, the
      transformation matrices are found. 
      @param option Not used. */
  virtual void Exec(Option_t* option="");
  /** Translate from detector coordinates to cartisian coordinates. 
      @param n Module number 
      @param sector Sector number in module 
      @param strip Strip number 
      @param xyz On return, the cartisian coordinates. */
  virtual void Detector2XYZ(UInt_t n, UInt_t sector, UInt_t strip, 
			    TVector3& xyz);

  Double_t GetMiddle() const { return fZMid; }
  /** Set Middle of detector setup 
      @param z @f$ z@f$ coordinate of setup (centimeters) */
  void SetMiddle(Double_t z); // *MENU*
  /** Set dimensions of the cave box
      @param x Full-length in @f$ x@f$ (centimeters)
      @param y Full-length in @f$ y@f$ (centimeters)
      @param z Full-length in @f$ z@f$ (centimeters) */
  void SetCaveDimensions(Double_t x, Double_t y, Double_t z); //*MENU*
  /** Set dimensions of the slat box
      @param x Full-length in @f$ x@f$ (centimeters)
      @param y Full-length in @f$ y@f$ (centimeters)
      @param z Full-length in @f$ z@f$ (centimeters) */
  void SetSlatDimensions(Double_t x, Double_t y, Double_t z); //*MENU*
  /** Set scintilator slat @f$ z@f$ coordinates (centimeters)
      @param z1 first slat @f$ z@f$ coordinates (centimeters)
      @param z2 second slat @f$ z@f$ coordinates (centimeters) */
  void SetSlatZ(Double_t z1, Double_t z2);  //*MENU*
  /** Set scintilator slat @f$ z@f$ coordinates (centimeters)
      @param zs first and second slat @f$ z@f$ coordinates (centimeters) */
  void SetSlatZ(Double_t* zs);  //*MENU*
  /** Set scintilator slat @f$ \phi@f$ rotation angles (degrees)
      @param phi1 first slat @f$ \phi@f$ rotation angles (degrees)
      @param phi2 second slat @f$ \phi@f$ rotation angles (degrees) */
  void SetSlatPhi(Double_t phi1, Double_t phi2);  //*MENU*
  /** Set scintilator slat @f$ \phi@f$  rotation angles (degrees)
      @param phis first and second slat @f$ \phi@f$ rotation angles (degrees)*/
  void SetSlatPhi(Double_t* phis);  //*MENU*
  /** Set @f$ z@f$ coordinate (centimeters) and @f$ \phi@f$ rotation
      angle (degrees) of the @f$ i^{th}@f$ scintilator slat */
  void SetSlatZPhi(Int_t i, Double_t z, Double_t phi);  //*MENU*
  /** Set dimensions of the transport box
      @param x Full-length in @f$ x@f$ (centimeters)
      @param y Full-length in @f$ y@f$ (centimeters)
      @param z Full-length in @f$ z@f$ (centimeters) */
  void SetTransportBoxDimensions(Double_t x, Double_t y, Double_t z);  //*MENU*
  /** Set the thickness of the material in the transport box. 
      @param thick Thickness in centimeters. */
  void SetTransportBoxThickness(Double_t thick);  //*MENU*
  /** Set dimensions of the storage box
      @param x Full-length in @f$ x@f$ (centimeters)
      @param y Full-length in @f$ y@f$ (centimeters)
      @param z Full-length in @f$ z@f$ (centimeters) */
  void SetStorageBoxDimensions(Double_t x, Double_t y, Double_t z);  //*MENU*
  /** Set the thickness of the material in the storage box. 
      @param thick Thickness in centimeters. */
  void SetStorageBoxThickness(Double_t thick);  //*MENU*
  /** Set offset of storage box, relative to sensor 
      @param off Offset (centimeters) */
  void SetStorageBoxOffset(Double_t off); // *MENU*
  
  /** Set the @f$ z@f$ coordinates of the modules.  This is to the
      front of the back-plane (aluminised surface). 
      @param z1 @f$ z@f$ coordinate of the first module (centimeters)
      @param z2 @f$ z@f$ coordinate of the second module (centimeters)
      @param z3 @f$ z@f$ coordinate of the third module (centimeters)
      @param z4 @f$ z@f$ coordinate of the fourth module (centimeters)
      @param z5 @f$ z@f$ coordinate of the fifth module (centimeters) */
  void SetZ(Double_t z1, Double_t z2, Double_t z3, Double_t z4, Double_t z5);  //*MENU*
  /** Set the @f$ z@f$ coordinates of the modules.  This is to the
      front of the back-plane (aluminised surface). 
      @param zs Array of 5 @f$ z@f$ coordinates (centimeters) */ 
  void SetZ(Double_t* zs);  //*MENU*    
  /** Set the @f$ \phi@f$ rotation angles (degrees) of the modules.
      This is around the @f$ z@f$ axis.  
      @param phis Array of 5 @f$ \phi@f$ angles (degrees) */ 
  void SetPhi(Double_t* phis);  //*MENU*
  /** Set the @f$ \phi@f$ rotation angles (degrees) of the modules.
      This is around the @f$ z@f$ axis.  
      @param p1 @f$ \phi@f$ rotation angle of the first module (degrees) 
      @param p2 @f$ \phi@f$ rotation angle of the second module (degrees)
      @param p3 @f$ \phi@f$ rotation angle of the third module (degrees) 
      @param p4 @f$ \phi@f$ rotation angle of the fourth module (degrees)
      @param p5 @f$ \phi@f$ rotation angle of the fifth module (degrees) */
  void SetPhi(Double_t p1, Double_t p2, Double_t p3, Double_t p4, Double_t p5);  //*MENU*
  /** Set the @f$ z@f$ coordinate and @f$ \phi@f$ rotation angle of
      the @f$ i^{th}@f$ module. 
      @param i Module number
      @param z @f$ z@f$ coordinate (centimeter)
      @param phi @f$ \phi@f$ rotation angle (degrees) */
  void SetZPhi(Int_t i, Double_t z, Double_t phi);  //*MENU*
  ClassDef(Geometry,0)
protected:
  Geometry();
  /** @{ 
      @name Miscellaneous */
  static Geometry*  fgInstance;
  /** List of transformation matrices */
  TObjArray*    fMatricies;
  /** Dimensions of cave box.  */
  Double_t      fZMid;
  /** @}  */

  /** @{ 
      @name Sensor parameters */
  /** Vertex of sensor shape  */
  TVector2    	fA;
  /** Vertex of sensor shape  */
  TVector2    	fB;
  /** Vertex of sensor shape  */
  TVector2    	fC;
  /** Opening angle of sensor  */
  Double_t	fTheta;
  /** Wafer radius  */
  Double_t	fWaferRadius;
  /** Thickness of silicon sensor  */
  Double_t	fSiThickness;
  /** Number of strips  */
  Int_t		fNStrips;
  /** Low radius  */
  Double_t	fLowR;
  /** @} */

  /** @{ 
      @name Hybrid parameters */
  /** Width of bonding pads on sensor  */
  Double_t      fBondingWidth;
  /** Thickness of chips on hybrid  */
  Double_t      fChipThickness;
  /** Thickness of copper in hybrid  */
  Double_t      fCopperThickness;
  /** Thickness of Hybrid PCB  */
  Double_t	fPrintboardThickness;
  /** Spacinng between hybrid and sensor  */
  Double_t      fSpacer;          

  /** @} */

  /** @{ 
      @name Leg parameters */
  /** Length of legs  */
  Double_t	fLegLength;
  /** Offset from edge of legs  */
  Double_t	fLegOffset;
  /** Radius of legs  */
  Double_t	fLegRadius;
  /** @} */
  
  /** @{
      @name Transport box parameters */
  /** Dimensions of transport box.  */
  TVector3      fTrans;
  /** Thickness of aluminum in transport box.  */
  Double_t      fTransThick;
  /** Support plate thickness  */
  Double_t      fPlateThick;
  /** @} */

  /** @{
      @name Storage box parameters */
  /** Dimensions of storage box.  */
  TVector3      fStore;
  /** Thickness of aluminum in storage box.  */
  Double_t      fStoreThick;
  /** @} */


  /** @{
      @name Trigger box parameters */
  /** Dimensions of trigger box.  */
  TVector3      fSlat;
  /** Thickness of aluminum in trigger box.  */
  Double_t      fZSlat[2];
  /** Thickness of aluminum in trigger box.  */
  Double_t      fPhiSlat[2];
  /** @} */

  /** @{ 
      @name Positions */ 
  /** Three planes in box */
  Double_t fZ[5];
  /** Rotation angles of storage boxes (in degrees) */
  Double_t fPhi[5];
  /** Offset in storage box */ 
  Double_t fZStoreOffset;
  /** @} */
  
  /** @{ 
      @name Tracking mediums */
  /** Air as a tracking medium */
  TGeoMedium* 	fAir;
  /** Aluminium as a tracking medium */
  TGeoMedium* 	fAl;
  /** Carbon as a tracking medium */
  TGeoMedium*   fCarbon;
  /** Chip (non active) as a tracking medium */
  TGeoMedium*   fChip;
  /** Copper (non active) as a tracking medium */
  TGeoMedium*   fCopper;
  /** PCB (G10) as a tracking medium */
  TGeoMedium*	fPCB;
  /** Plastic as a tracking medium */
  TGeoMedium*	fPlastic;
  /** Silicon (active) as a tracking medium */
  TGeoMedium*	fSi;
  /** Vacuum as a tracking medium */
  TGeoMedium*	fVacuum;
  /** Scintilator as a tracking medium */
  TGeoMedium*	fScint;
  /** @} */


  /** Make the geomtry of a module. 
      @return A pointer to a TGeoVolumeAssembly */
  TGeoVolume* MakeModule();
  void MakeSlat(TGeoVolume* mother);
  void MakeTrans(TGeoVolume* mother);
  void MakeStore(TGeoVolume* mother);
};

#endif
//
//  EOF
// 
