//____________________________________________________________________ 
//  
// $Id: Detector.cxx,v 1.3 2006-06-20 23:59:43 hehi Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    astrid/Detector.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementaion file for Detector
*/
#ifndef Detector_h
# include "Detector.h"
#endif
#ifndef Hit_h
# include "Hit.h"
#endif
#ifndef Geometry_h
# include "Geometry.h"
#endif
#ifndef Simulation_Main
# include <simulation/Main.h>
#endif
#ifndef Simulation_Hit
# include <simulation/Hit.h>
#endif
#ifndef ROOT_TLorentzVector
# include <TLorentzVector.h>
#endif
#ifndef ROOT_TParticle
# include <TParticle.h>
#endif
#ifndef ROOT_TVector2
# include <TVector2.h>
#endif
#ifndef ROOT_TDatabasePDG
# include <TDatabasePDG.h>
#endif
#ifndef ROOT_TVirtualMC
# include <TVirtualMC.h>
#endif
#ifndef ROOT_TGeoVolume
# include <TGeoVolume.h>
#endif
#ifndef ROOT_TGeoMedium
# include <TGeoMedium.h>
#endif
#ifndef ROOT_TGeoManager
# include <TGeoManager.h>
#endif
#ifndef ROOT_TArrayI
# include <TArrayI.h>
#endif
#ifndef ROOT_TTree
# include <TTree.h>
#endif

//____________________________________________________________________
Detector::Detector()
  : Simulation::Task("Detector", "FMD Detector")
{
  fCache = new TClonesArray("Hit");
  // CreateDefaultHitArray();
}

//____________________________________________________________________
void 
Detector::Initialize(Option_t* option)
{
  Simulation::Task::Initialize(option);
  // Silicon 
  RegisterMedium(gGeoManager->GetMedium("Si"));
}


//____________________________________________________________________
void
Detector::Register(Option_t* option)
{
  TTree* tree = 0;
  if ((tree = GetBaseTree("Hits"))) fBranch = tree->Branch(GetName(), &fCache);
  RegisterVolume(gGeoManager->GetVolume("Strip"));
}

//____________________________________________________________________
void
Detector::Step()
{
  TVirtualMC* mc = TVirtualMC::GetMC();
  
  // If track is dead, do nothing 
  if (!mc->IsTrackAlive()) return;
  // If the particle isn't charged, do nothing. 
  if (TMath::Abs(mc->TrackCharge()) <= 0) return;

  // check that we're in an active volume. 
  Int_t copy;
  Int_t vol = mc->CurrentVolID(copy);
  if (!IsSensitive(vol)) return;
  
  // CreateDefaultHit();

  // Static variables. 
  static Float_t energyLoss;
  static TLorentzVector v;
  static TLorentzVector p;

  // If the track is entering, reset variables. 
  if (mc->IsTrackEntering()) {
    mc->TrackPosition(v);
    mc->TrackMomentum(p);
    energyLoss = 0;
  }
  // If we're inside an active volume, increment the energy loss. 
  if (mc->IsTrackInside())   energyLoss += 1000 * mc->Edep();

  // If the track is exiting, disappering, or stopped, make a hit. 
  if (mc->IsTrackExiting()|| mc->IsTrackDisappeared()|| mc->IsTrackStop()) {
    // Get some stuff from the VMC, like particle momentum, track
    // number, and so on
    Int_t    track = mc->GetStack()->GetCurrentTrackNumber();
    Double_t edep  = mc->Edep();
    Int_t    pdg   = mc->TrackPid();

    // Calculate length through active element. 
    TLorentzVector cur;
    mc->TrackPosition(cur);
    TVector3 vv(cur.Vect());
    vv -= v.Vect();
    Double_t len = vv.Mag();
    
    // Get the detector coordinates. 
    UShort_t strip = copy - 1;
    Int_t sector;
    mc->CurrentVolOffID(1,sector);
    Int_t module;
    mc->CurrentVolOffID(4, module);
    

    // Now, we create a hit, if one does not already exist for the
    // current track number.
    AddHit(track, module, sector, strip, 1000 * edep, v, p, pdg, len);
    mc->GetStack()->GetCurrentTrack()->SetBit(Simulation::kKeepBit);
  }
}

//____________________________________________________________________
void
Detector::AddHit(Int_t track, UShort_t module, UShort_t sector, 
		 UShort_t strip, Float_t eloss, 
		 const TLorentzVector& v, const TLorentzVector& p, 
		 Int_t pdg, Float_t len)
{
  TClonesArray* hits  = static_cast<TClonesArray*>(fCache);
  Int_t         nhits = hits->GetEntriesFast();
  Hit*          hit   = 0;

  for (Int_t i = 0; i < nhits; i++) {
    // There may be holes in the cache
    if (!hits->At(i)) continue;
    hit = static_cast<Hit*>(hits->At(i));
    if (track    == hit->NTrack()  &&
        module   == hit->Sensor()  &&
        sector   == hit->Sector()  &&
        strip    == hit->Strip()) {
      Debug(15,"AddHit", "Warning: There already exists a hit for track "
            "# %d in Module%d[%2d,%3d], updating that one",
            track, module, sector, strip);
      hit->AddToEnergyLoss(eloss);
      hit->AddToLength(len);
    }
    else
      hit = 0;
  }
  if (!hit) {
    Debug(15, "AddHit", "Adding a hit from track %d in Module%d[%2d,%3d]",
          track, module, sector, strip);
    hit = new ((*hits)[nhits]) Hit(module, sector, strip, track, v, p, 
				   eloss, pdg, len);
  }
}

  

//____________________________________________________________________
//
// EOF
//

  
