//____________________________________________________________________ 
//  
// $Id: Hit.cxx,v 1.2 2006-06-20 23:59:43 hehi Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    astrid/Hit.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementaion file for Hit
*/
#ifndef Hit_h
# include "Hit.h"
#endif
#ifndef ROOT_TString
# include <TString.h>
#endif
#ifndef ROOT_TDatabasePDG
# include <TDatabasePDG.h>
#endif
#ifndef __IOSTREAM__
# include <iostream>
#endif
#ifndef __IOMANIP__
# include <iomanip>
#endif

//____________________________________________________________________
const char*
Hit::GetTitle() const
{
  if (fTitle.IsNull()) 
    fTitle = Form("Sensor%d[%2d,%3d]: %10.8f MeV / %7.5f cm", 
		  fSensor, fSector, fStrip, fEnergyLoss, fLength);
  return fTitle.Data();
}

//____________________________________________________________________
void
Hit::Print(Option_t* option) const
{
  std::cout << "Hit: " << GetTitle() << std::endl;
  TString opt(option);
  if (opt.Contains("p", TString::kIgnoreCase)) {
    TDatabasePDG* pdgDb = TDatabasePDG::Instance();
    TParticlePDG* pdgP  = pdgDb->GetParticle(fPdg);
    std::cout << "  Particle: " << pdgP->GetName()
              << " (track # " << fNTrack << ") x=["
              << std::setw(10) << fV.X() << ","
              << std::setw(10) << fV.Y() << ","
              << std::setw(10) << fV.Z() << ","
              << std::setw(10) << fV.T() << "] p=["
              << std::setw(10) << fP.Px() << ","
              << std::setw(10) << fP.Py() << ","
              << std::setw(10) << fP.Pz() << ","
              << std::setw(10) << fP.E() << "]" << std::endl;
  }
}

//____________________________________________________________________
//
// EOF
//
