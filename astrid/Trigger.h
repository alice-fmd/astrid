// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: Trigger.h,v 1.3 2006-06-20 23:59:43 hehi Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    astrid/Trigger.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Trigger
*/
#ifndef Trigger_h
#define Trigger_h
#ifndef Simulation_Task
# include <simulation/Task.h>
#endif
#ifndef ROOT_TArrayI
# include <TArrayI.h>
#endif
class TVector2;
class TGeoVolume;
class TGeoMedium;


/** @brief class to handle hits in scinitilator slats

    @image html event.png
 */
class Trigger :  public Simulation::Task 
{
public:
  /** Constructor */
  Trigger();
  /** Register active medium
      @param option Not used */
  virtual void Initialize(Option_t* option="");
  /** Register active volume 
      @param option Not used. */
  virtual void Register(Option_t* option="");
  /** Process hits in slats */
  virtual void Step();
  ClassDef(Trigger,0)
protected:
};

#endif
//
//  EOF
// 
