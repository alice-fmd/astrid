//____________________________________________________________________ 
//  
// $Id: Geometry.cxx,v 1.3 2006-06-20 23:59:43 hehi Exp $ 
//
//  Example offline project
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    astrid/Geometry.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementaion file for Geometry
*/
#ifndef Geometry_h
# include "Geometry.h"
#endif
#ifndef Simulation_Main
# include <simulation/Main.h>
#endif
#ifndef ROOT_TGeoVolume
# include <TGeoVolume.h>
#endif
#ifndef ROOT_TGeoMaterial
# include <TGeoMaterial.h>
#endif
#ifndef ROOT_TGeoMedium
# include <TGeoMedium.h>
#endif
#ifndef ROOT_TGeoMatrix
# include <TGeoMatrix.h>
#endif
#ifndef ROOT_TGeoXtru
# include <TGeoXtru.h>
#endif
#ifndef ROOT_TGeoPcon
# include <TGeoPcon.h>
#endif
#ifndef ROOT_TGeoTube
# include <TGeoTube.h>
#endif
#ifndef ROOT_TGeoManager
# include <TGeoManager.h>
#endif
#ifndef ROOT_TVector3
# include <TVector3.h>
#endif
//
//
//
// -------------------------------------
// Ring I
//  BondingWidth        :   0.5000
//  WaferRadius         :   6.7000
//  SiThickness         :   0.0300
//  LowR                :   4.3000
//  HighR               :  17.2000
//  Theta               :  18.0000
//  NStrips             :      512
//  RingDepth           :   2.1600
//  LegRadius           :   0.5000
//  LegLength           :   1.0000
//  LegOffset           :   2.0000
//  ModuleSpacing       :   1.0000
//  PrintboardThickness :   0.1000
//  SpacerHeight        :   0.0300
//  Vertex # 0          :   4.3000,  -1.3972
//  Vertex # 1          :  15.3327,  -4.9819
//  Vertex # 2          :  17.2000,  -2.1452
//  Vertex # 3          :  17.2000,   2.1452
//  Vertex # 4          :  15.3327,   4.9819
//  Vertex # 5          :   4.3000,   1.3972
// -------------------------------------
// Ring O
//  BondingWidth        :   0.5000
//  WaferRadius         :   6.7000
//  SiThickness         :   0.0300
//  LowR                :  15.6000
//  HighR               :  28.0000
//  Theta               :   9.0000
//  NStrips             :      256
//  RingDepth           :   2.1600
//  LegRadius           :   0.5000
//  LegLength           :   1.0000
//  LegOffset           :   2.0000
//  ModuleSpacing       :   1.0000
//  PrintboardThickness :   0.1000
//  SpacerHeight        :   0.0300
//  Vertex # 0          :  15.6000,  -2.4708
//  Vertex # 1          :  26.9872,  -4.2744
//  Vertex # 2          :  28.0000,  -2.6065
//  Vertex # 3          :  28.0000,   2.6065
//  Vertex # 4          :  26.9872,   4.2744
//  Vertex # 5          :  15.6000,   2.4708
// -------------------------------------
// Detector 1
//  InnerZ                 : 340.0000
//  OuterZ                 :   0.0000
//  HoneycombThickness     :   1.0000
//  AlThickness            :   0.1000
//  InnerHoneyLowR         :   5.3000
//  InnerHoneyHighR        :  18.2000
//  OuterHoneyLowR         :   0.0000
//  OuterHoneyHighR        :   0.0000
// -------------------------------------
// Detector 2
//  InnerZ                 :  83.4000
//  OuterZ                 :  75.2000
//  HoneycombThickness     :   1.0000
//  AlThickness            :   0.1000
//  InnerHoneyLowR         :   5.3000
//  InnerHoneyHighR        :  18.2000
//  OuterHoneyLowR         :  16.6000
//  OuterHoneyHighR        :  29.0000
// -------------------------------------
// Detector 3
//  InnerZ                 : -62.8000
//  OuterZ                 : -75.2000
//  HoneycombThickness     :   1.0000
//  AlThickness            :   0.1000
//  InnerHoneyLowR         :   5.3000
//  InnerHoneyHighR        :  18.2000
//  OuterHoneyLowR         :  16.6000
//  OuterHoneyHighR        :  29.0000
// -------------------------------------

//____________________________________________________________________
Geometry* Geometry::fgInstance = 0;

//____________________________________________________________________
Geometry* Geometry::Instance()
{
  if (!fgInstance) fgInstance = new Geometry;
  return fgInstance;
}

//____________________________________________________________________
Geometry::Geometry()
  : Framework::Task("Geometry", "FMD Geometry"),
    fA( 4.3000,   1.3972),
    fB(17.2000,   2.1452),
    fC(15.3327,   4.9819), 
    fMatricies(0), 
    fTrans(20, 20, 50),
    fStore(18, 18,  8), 
    fSlat(1,1, 30)
{
  fWaferRadius            =   6.7000;
  fSiThickness            =   0.0300;
  fLowR                   =   4.3000;
  fTheta                  =  18.0000;
  fNStrips                = 512;

  fLegRadius              =   0.5000;
  fLegLength              =   1.0000;
  fLegOffset              =   2.0000;

  fPrintboardThickness    =   0.1000;
  fBondingWidth           =   0.5000;
  fCopperThickness        =   0.01;
  fChipThickness          =   0.01;
  fSpacer                 =   0.10;

  fTransThick             =   0.1;
  fPlateThick             =   0.1;

  fStoreThick             =   0.3;
  fZStoreOffset	          =   1.0;
  
  fZMid                   = 200.0;

  fZ[0]                   = -30.0;
  fZ[1]                   = -20.0;
  fZ[2]                   =   0.0;
  fZ[3]                   =  15.0;
  fZ[4]                   =  30.0;
  
  fPhi[0]                 = 180.0;
  fPhi[1]                 =  90.0;
  fPhi[2]                 =  90.0;
  fPhi[3]                 =  90.0;
  fPhi[4]                 =   0.0;
  

  fZSlat[0]               = -50.0;
  fZSlat[1]               =  50.0;
  fPhiSlat[0]             =   0.0;
  fPhiSlat[1]             =   0.0;
  
}

//____________________________________________________________________
void 
Geometry::Initialize(Option_t* option)
{
  Framework::Task::Initialize(option);
  Double_t mMax  = 0;
  Double_t mType = 0;
  
  // Air 
  Double_t pAir[]     = { 0., mType, mMax, 1.,  .001, 1., .001, .001 };
  TGeoMixture* fmdAir = new TGeoMixture("Air", 4, .00120479);
  fmdAir->DefineElement(0, 12.0107,  6., 0.000124);
  fmdAir->DefineElement(1, 14.0067,  7., 0.755267);
  fmdAir->DefineElement(2, 15.9994,  8., 0.231781);
  fmdAir->DefineElement(3, 39.948,  18., 0.012827);
  fmdAir->SetTransparency('0');
  fAir = new TGeoMedium("Air", 1, fmdAir,  pAir);

  // Silicon 
  Double_t pSi[]      = { 1., mType, mMax, 1.,  .001, 1., .001, .001 };
  TGeoMaterial* fmdSi = new TGeoMaterial("Si", 28.0855, 14, 2.33);
  fmdSi->SetFillColor(2);
  fSi = new TGeoMedium("Si", 1, fmdSi,  pSi);

  // Vacumm 
  Double_t pVacuum[]  = { 0., mType, mMax, 10,  .01, .1, .003, .003 };
  TGeoMaterial* fmdVacuum = new TGeoMaterial("Vacuum",1e-16,1e-16,1e-16);
  fmdVacuum->SetTransparency('0');
  fVacuum = new TGeoMedium("Vacuum", 1, fmdVacuum,pVacuum);


  // PCB 
  Double_t pPCB[]     = { 0., mType, mMax, 1.,  .001, 1., .001, .001 };
  TGeoMixture* fmdPCB = new TGeoMixture("PCB", 14, 1.8);
  fmdPCB->DefineElement(0,  28.0855,   14, 0.15144894);
  fmdPCB->DefineElement(1,  40.078,    20, 0.08147477);
  fmdPCB->DefineElement(2,  26.981538, 13, 0.04128158);
  fmdPCB->DefineElement(3,  24.305,    12, 0.00904554);
  fmdPCB->DefineElement(4,  10.811,     5, 0.01397570);
  fmdPCB->DefineElement(5,  47.867,    22, 0.00287685);
  fmdPCB->DefineElement(6,  22.98977,  11, 0.00445114);
  fmdPCB->DefineElement(7,  39.0983,   19, 0.00498089);
  fmdPCB->DefineElement(8,  55.845,    26, 0.00209828);
  fmdPCB->DefineElement(9,  18.9984,    9, 0.00420000);
  fmdPCB->DefineElement(10, 15.9994,    8, 0.36043788);
  fmdPCB->DefineElement(11, 12.0107,    6, 0.27529425);
  fmdPCB->DefineElement(12, 14.0067,    7, 0.01415852);
  fmdPCB->DefineElement(13,  1.00794,   1, 0.03427566);
  fmdPCB->SetFillColor(7);
  fPCB = new TGeoMedium("PCB", 1, fmdPCB,  pPCB);

  // Chip 
  Double_t pChip[]  = { 0., mType, mMax, 10., .01,  1., .003, .003 };
  TGeoMixture* fmdChip = new TGeoMixture("Chip", 6, 2.36436);
  fmdChip->DefineElement(0,  12.0107,   6, 0.039730642);
  fmdChip->DefineElement(1,  14.0067,   7, 0.001396798);
  fmdChip->DefineElement(2,  15.9994,   8, 0.01169634);
  fmdChip->DefineElement(3,   1.00794,  1, 0.004367771);
  fmdChip->DefineElement(4,  28.0855,  14, 0.844665);
  fmdChip->DefineElement(5, 107.8682,  47, 0.0981434490);
  fmdChip->SetFillColor(4);
  fChip = new TGeoMedium("Chip", 1, fmdChip, pChip);

  // Carbon 
  Double_t pC[]       = { 0., mType, mMax, 10., .01,  1., .003, .003 };
  TGeoMaterial* fmdC = new TGeoMaterial("C", 12.011, 6, 2.265);
  fmdC->SetFillColor(6);
  fCarbon = new TGeoMedium("C", 1, fmdC, pC);
  
  // Plastic 
  Double_t pPlastic[] = { 0., mType, mMax, 10., .01,  1., .003, .003 };
  TGeoMixture* fmdPlastic = new TGeoMixture("Plastic", 2, 1.03);
  fmdPlastic->DefineElement(0,  1.01,   1, .5);
  fmdPlastic->DefineElement(1,  12.01,  6, .5);
  fmdPlastic->SetFillColor(4);
  fPlastic = new TGeoMedium("Plastic", 1, fmdPlastic,  pPlastic);

  // Aluminium 
  Double_t pAl[]  = { 0., mType, mMax, 10., .001,  -1., .003, .003 };
  TGeoMaterial* fmdAl = new TGeoMaterial("Al", 26.981539, 13, 2.7);
  fmdAl->SetFillColor(3);
  fAl = new TGeoMedium("Al", 1, fmdAl, pAl);

  // Copper 
  Double_t pCopper[]  = { 0., mType, mMax, 10., .01,  1., .003, .003 };
  TGeoMaterial* fmdCopper = new TGeoMaterial("Copper", 63.546,  29.,8.96);
  fmdCopper->SetFillColor(3);
  fCopper = new TGeoMedium("Copper",  1, fmdCopper, pCopper);


  // Mixture scint
  Double_t pScint[] = { 1, 0, 0, 20, 1e+10, 0.0308578, 0.01, 0.0127021 };
  TGeoMixture* matScint = new TGeoMixture("Scintilator", 2, 1.032);
  matScint->DefineElement(0, 1.008, 1, 0.0774908);     // H
  matScint->DefineElement(1, 12, 6, 0.922509);         // C
  fScint= new TGeoMedium("Scintilator", 0, matScint, pScint);
}

//____________________________________________________________________
TGeoVolume*
Geometry::MakeModule() 
{
  Double_t xv[6] = { fA.X(), fC.X(), fB.X(),  fB.X(),  fC.X(),  fA.X() };
  Double_t yv[6] = { fA.Y(), fC.Y(), fB.Y(), -fB.Y(), -fC.Y(), -fA.Y() };
  Double_t rmax     = fB.Mod();
  Double_t stripoff = fA.Mod();
  Double_t dstrip   = (rmax - stripoff) / fNStrips;

  
  // Sensor 
  TGeoXtru* sensorShape = new TGeoXtru(2);
  sensorShape->DefinePolygon(6, xv, yv);
  sensorShape->DefineSection(0, - fSiThickness/2);
  sensorShape->DefineSection(1, fSiThickness/2);
  TGeoVolume* sensorVolume= new TGeoVolume("Sensor",sensorShape,fSi);
  sensorVolume->VisibleDaughters(kFALSE);
  sensorVolume->SetVisibility(kTRUE);

  // Virtual volume shape to divide 
  TGeoTubeSeg* activeShape   = new TGeoTubeSeg(fLowR, rmax, fSiThickness/2, 
					       - fTheta, fTheta);
  TGeoVolume*  activeVolume  = new TGeoVolume("Active", activeShape,fSi);
  sensorVolume->AddNodeOverlap(activeVolume, 0);
  // activeVolume->SetTransparency(0x3f);
  TGeoVolume* sectorVolume   = activeVolume->Divide("Sector",2,2,-fTheta,
						    0,0,"N");
  TGeoVolume* stripVolume    = sectorVolume->Divide("Strip",1,fNStrips,
						    stripoff,dstrip,0,"SX");

  // Position
  Double_t x, y, z;
  
  // Make PCB volume 
  for (Int_t i = 0; i < 3; i++) yv[i] -= fBondingWidth;
  for (Int_t i = 3; i < 6; i++) yv[i] += fBondingWidth;
  Double_t off = (TMath::Tan(TMath::Pi() * fTheta / 180) * fBondingWidth);

  // PCB layer 
  TGeoXtru* pcbShape      = new TGeoXtru(2);
  pcbShape->DefinePolygon(6, xv, yv);
  pcbShape->DefineSection(0, - fPrintboardThickness/2);
  pcbShape->DefineSection(1, fPrintboardThickness/2);
  TGeoVolume* pcbVolume   = new TGeoVolume("HybridPcb",pcbShape,fPCB);

  // Copper layer
  TGeoXtru* cuShape       = new TGeoXtru(2);
  cuShape->DefinePolygon(6, xv, yv);
  cuShape->DefineSection(0, - fCopperThickness/2);
  cuShape->DefineSection(1, fCopperThickness/2);
  TGeoVolume* cuVolume    = new TGeoVolume("HybridCo",cuShape,fCopper);

  // Chip layer
  TGeoXtru*   chipShape   = new TGeoXtru(2);
  chipShape->DefinePolygon(6, xv, yv);
  chipShape->DefineSection(0, - fChipThickness/2);
  chipShape->DefineSection(1, fChipThickness/2);
  TGeoVolume* chipVolume = new TGeoVolume("HybridChip",chipShape,fChip);

  // Short leg shape 
  TGeoTube*   shortLegShape  = new TGeoTube(0, fLegRadius, fLegLength / 2);
  TGeoVolume* shortLegVolume = new TGeoVolume("Leg", shortLegShape, fPlastic);
  shortLegVolume->SetVisibility(kFALSE);
  
  // Make a front volume 
  TGeoVolume* frontVolume    =  new TGeoVolumeAssembly("Module");
  z                          =  fSiThickness / 2;
  frontVolume->AddNode(sensorVolume, 0, new TGeoTranslation(0, 0, z));
  z                          += ((fPrintboardThickness + fSiThickness) / 2 
				 + fSpacer);
  frontVolume->AddNode(pcbVolume, 0, new TGeoTranslation(0, 0, z));
  z                          += ((fPrintboardThickness + fCopperThickness) / 2 
				 + fSpacer);
  frontVolume->AddNode(cuVolume, 0, new TGeoTranslation(0, 0, z));
  z                          += (fCopperThickness + fChipThickness) / 2;
  frontVolume->AddNode(chipVolume, 0, new TGeoTranslation(0, 0, z));
  z                          += (fChipThickness + fLegLength) / 2;
  x                          =  fA.X() + fLegOffset + fLegRadius;
  y                          =  0;
  frontVolume->AddNode(shortLegVolume, 0, new TGeoTranslation(x, y, z));
  x                          =  fC.X();
  y                          =  fC.Y() - fLegOffset - fLegRadius - off;
  frontVolume->AddNode(shortLegVolume, 1, new TGeoTranslation(x,y,z));
  y                          =  -y;
  frontVolume->AddNode(shortLegVolume, 2, new TGeoTranslation(x,y,z));

  // Return the volume 
  return frontVolume;
}

//____________________________________________________________________
void
Geometry::MakeTrans(TGeoVolume* mother) 
{
  // Make a transport box 
  Double_t dx = fTrans.X() / 2;
  Double_t dy = fTrans.Y() / 2;
  Double_t dz = fTrans.Z() / 2;
  Double_t dt = fTransThick / 2;
  // Sides, bottom, top, and ends 
  TGeoBBox*   sideShape   =  new TGeoBBox(dt, dy, dz);
  TGeoVolume* sideVol     =  new TGeoVolume("TransSide", sideShape, fAl);
  dx                      -= fTransThick;
  TGeoBBox*   bottomShape =  new TGeoBBox(dx, dt, dz);
  TGeoVolume* bottomVol   =  new TGeoVolume("TransBottom", bottomShape, fAl);
  TGeoBBox*   topShape    =  new TGeoBBox(dx, dt, dz);
  TGeoVolume* topVol      =  new TGeoVolume("TransTop", topShape, fPlastic);
  dy                      -= fTransThick;
  dz                      -= fTransThick;
  TGeoBBox*   endShape    =  new TGeoBBox(dx, dy, dt);
  TGeoVolume* endVol      =  new TGeoVolume("TransEnd", endShape, fAl);
  // Make plate volume 
  TGeoBBox*   plateShape  =  new TGeoBBox(dx, dy, fPlateThick / 2);
  TGeoVolume* plateVol    =  new TGeoVolume("TransPlate", plateShape, fAl);

  // Place volumes in mother 
  dx                      += fTransThick/2;
  dy                      += fTransThick/2;
  dz                      += fTransThick/2;
  mother->AddNode(sideVol,   1, new TGeoTranslation(-dx,  0,  0));
  mother->AddNode(sideVol,   2, new TGeoTranslation(+dx,  0,  0));
  mother->AddNode(bottomVol, 1, new TGeoTranslation(  0,-dy,  0));
  mother->AddNode(topVol,    1, new TGeoTranslation(  0,+dy,  0));
  mother->AddNode(endVol,    1, new TGeoTranslation(  0,  0,-dz));
  mother->AddNode(endVol,    2, new TGeoTranslation(  0,  0,+dz));
  
  Double_t moduleThick = (fPrintboardThickness + fCopperThickness 
			  + fChipThickness + fSiThickness + fSpacer 
			  + fLegLength);
  for (size_t i = 1; i <= 3; i++) {
    Double_t z = fZ[i] + moduleThick + fPlateThick / 2;
    mother->AddNode(plateVol, i+1, new TGeoTranslation(0,0,z));
  }
}

//____________________________________________________________________
void
Geometry::MakeStore(TGeoVolume* mother) 
{
  // Make a storeport box 
  Double_t dx = fStore.X() / 2;
  Double_t dy = fStore.Y() / 2;
  Double_t dz = fStore.Z() / 2;
  Double_t dt = fStoreThick / 2;
  // Sides, bottom, top, and ends 
  TGeoBBox*   sideShape   =  new TGeoBBox(dt, dy, dz);
  TGeoVolume* sideVol     =  new TGeoVolume("StoreSide", sideShape, fAl);
  dx                      -= fStoreThick;
  TGeoBBox*   topbotShape =  new TGeoBBox(dx, dt, dz);
  TGeoVolume* topbotVol   =  new TGeoVolume("StoreTopBot", topbotShape, fAl);
  dy                      -= fStoreThick;
  dz                      -= fStoreThick;
  TGeoBBox*   endShape    =  new TGeoBBox(dx, dy, dt);
  TGeoVolume* endVol      =  new TGeoVolume("StoreEnd", endShape, fAl);
  // Make plate volume 
  TGeoBBox*   plateShape  =  new TGeoBBox(dx, dy, fPlateThick / 2);
  TGeoVolume* plateVol    =  new TGeoVolume("StorePlate", plateShape, fAl);

  // Place volumes in mother 
  for (size_t i = 0; i < 2; i++) {
    Int_t    id = (i == 0 ? 0 : 4);
    Double_t z  = fZ[id];
    Double_t mz = z + fStore.Z() / 2 - fZStoreOffset;
    dx          = fStore.X() / 2 - fStoreThick / 2;
    dy          = fStore.Y() / 2 - fStoreThick / 2;
    dz          = fStore.Z() / 2 - fStoreThick / 2;
    mother->AddNode(sideVol,   1, new TGeoTranslation(-dx,  0,   mz));
    mother->AddNode(sideVol,   2, new TGeoTranslation(+dx,  0,   mz));
    mother->AddNode(topbotVol, 1, new TGeoTranslation(  0,-dy,   mz));
    mother->AddNode(topbotVol, 2, new TGeoTranslation(  0,+dy,   mz));
    mother->AddNode(endVol,    1, new TGeoTranslation(  0,  0,mz-dz));
    mother->AddNode(endVol,    2, new TGeoTranslation(  0,  0,mz+dz));
  
    Double_t moduleThick = (fPrintboardThickness + fCopperThickness 
			    + fChipThickness + fSiThickness + fSpacer 
			    + fLegLength);
    z = fZ[id] + moduleThick + fPlateThick / 2;
    mother->AddNode(plateVol, id+1, new TGeoTranslation(0,0,z));
  }
}
  
//____________________________________________________________________
void
Geometry::MakeSlat(TGeoVolume* mother) 
{
  // Make scintilator slats for the trigger 
  Double_t dx = fSlat.Y() / 2;
  Double_t dy = fSlat.Z() / 2;
  Double_t dz = fSlat.X() / 2;
  TGeoBBox* slatShape    = new TGeoBBox(dx, dy, dz);
  TGeoVolume* slatVolume = new TGeoVolume("Slat", slatShape, fScint);

  for (Int_t i = 0; i < 2; i++) {
    TGeoCombiTrans* matrix = new TGeoCombiTrans(0, 0, fZSlat[i], 0);
    matrix->RotateZ(fPhiSlat[i]);
    mother->AddNode(slatVolume, i+1, matrix);
  }
}

//____________________________________________________________________
void
Geometry::Register(Option_t* option)
{
  // Framework::Task::Register(option);
  Double_t dx = .75 * std::max(std::max(fSlat.X(), fTrans.X()), fStore.X());
  Double_t dy = .75 * std::max(std::max(fSlat.Y(), fTrans.Y()), fStore.Y());
  Double_t dz = .55 * (fZMid + fZSlat[1]);
  Double_t o[] = { 0, 0, dz };
  TGeoBBox* caveShape = new TGeoBBox(dx, dy, dz, o);
  TGeoVolume* caveVolume = new TGeoVolume("Cave", caveShape, fAir);
  gGeoManager->SetTopVolume(caveVolume);

  if (gGeoManager->GetVolume("Module")) return;

  // Make a volume that holds all the stuff 
  TGeoVolume* allVolume = new TGeoVolumeAssembly("All");

  // Get the module volume 
  TGeoVolume* moduleVolume = MakeModule();

  dx = (fB.X() - fA.X()) / 2 + fLowR;
  for (Int_t i = 0; i < 5; i++) {
    TGeoCombiTrans* matrix = new TGeoCombiTrans(-dx, 0, fZ[i], 0);
    matrix->RotateZ(fPhi[i]);
    allVolume->AddNode(moduleVolume, i+1, matrix);
  }
    
  MakeTrans(allVolume);
  MakeStore(allVolume);
  MakeSlat(allVolume);
  
  caveVolume->AddNode(allVolume, 0, new TGeoTranslation(0, 0, fZMid));
}

//____________________________________________________________________
void
Geometry::Exec(Option_t* option)
{
  static Bool_t first = kTRUE;
  if (!first)                       return;
  if (!gGeoManager)                 return;
  if (!gGeoManager->GetTopVolume()) return;
  if (!gGeoManager->IsClosed())     return;
  
  if (!fMatricies) fMatricies = new TObjArray;
  TGeoIterator next(gGeoManager->GetTopVolume());
  TGeoNode* node = 0;

  while ((node = static_cast<TGeoNode*>(next()))) {
    // node->Print();
    TString name(node->GetName());
    if (name == "Sensor") {
      // We got an FMD module 
      Int_t       n = node->GetNumber();
      TGeoMatrix* m = new TGeoCombiTrans(*(next.GetCurrentMatrix()));
      Verbose(1, "Exec", "Adding node %s w/# %d and matrix %p", 
	      node->GetName(),n, m);
      // m->Print();
      fMatricies->AddAt(const_cast<TGeoMatrix*>(m), n);
    }
  }
  first = kFALSE;
}

#define DEGRAD TMath::Pi()/180.
//____________________________________________________________________
void 
Geometry::Detector2XYZ(UInt_t n, UInt_t sector, UInt_t strip, TVector3& xyz)
{
  UInt_t   mod      = n;
  if (!fMatricies) {
    Warning("Detector2XYZ", "No matricies");
    return;
  }
  TGeoMatrix* m = static_cast<TGeoMatrix*>(fMatricies->At(mod));
  if (!m) {
    Warning("Detector2XYZ", "No matrix found for module %d", mod);
    return;
  }
  Debug(10, "Detector2XYZ", "Transforming (%2d,%3d)", sector, strip);
  Double_t rmax     = fB.Mod();
  Double_t stripoff = fA.Mod();
  Double_t dstrip   = (rmax - stripoff) / fNStrips;
  Double_t r        = (strip + .5) * dstrip + stripoff; // fLowR
  Double_t theta    = ((sector % 2) - .5) * fTheta;
  Double_t modThick = (fSiThickness 
		       + fPrintboardThickness 
		       + fCopperThickness
		       + fChipThickness 
		       + fSpacer);
  Debug(10,"Detector2XYZ", "Radius %7.3f, angle %7.3f (%f, %f)", r, theta, 
       fLowR, stripoff);
  Double_t local[] = {
    r * TMath::Cos(theta * DEGRAD), 
    r * TMath::Sin(theta * DEGRAD), 
    -modThick + fSiThickness / 2
  };
  Double_t master[3];
  Debug(10, "Detector2XYZ", "Local (%7.3f,%7.3f,%7.3f)", 
       local[0], local[1], local[2]);
  m->LocalToMaster(local, master);
  Debug(10, "Detector2XYZ", "Master (%7.3f,%7.3f,%7.3f)", 
       master[0], master[1], master[2]);
  xyz.SetXYZ(master[0], master[1], master[2]);
}

//____________________________________________________________________
void
Geometry::SetMiddle(Double_t z) 
{
  fZMid = z;
}

//____________________________________________________________________
void
Geometry::SetSlatDimensions(Double_t x, Double_t y, Double_t z) 
{
  fSlat.SetXYZ(x,y,z);
}
//____________________________________________________________________
void
Geometry::SetSlatZ(Double_t z1, Double_t z2)  
{
  fZSlat[0] = z1;
  fZSlat[1] = z2;
}
//____________________________________________________________________
void
Geometry::SetSlatZ(Double_t* zs)  
{ 
  fZSlat[0] = zs[0]; 
  fZSlat[1] = zs[1]; 
}
//____________________________________________________________________
void
Geometry::SetSlatPhi(Double_t phi1, Double_t phi2)  
{
  fPhiSlat[0] = phi1;
  fPhiSlat[1] = phi2;
}
//____________________________________________________________________
void
Geometry::SetSlatPhi(Double_t* phis)  
{ 
  fPhiSlat[0] = phis[0]; 
  fPhiSlat[1] = phis[1]; 
}
//____________________________________________________________________
void
Geometry::SetSlatZPhi(Int_t i, Double_t z, Double_t phi)  
{
  if (i < 0 || i > 1) return;
  fZSlat[i]   = z;
  fPhiSlat[i] = phi;
}
//____________________________________________________________________
void
Geometry::SetTransportBoxDimensions(Double_t x, Double_t y, Double_t z)  
{
  fTrans.SetXYZ(x,y,z);
}
//____________________________________________________________________
void
Geometry::SetTransportBoxThickness(Double_t thick)  
{ 
  fTransThick = thick; 
}
//____________________________________________________________________
void
Geometry::SetStorageBoxDimensions(Double_t x, Double_t y, Double_t z)  
{
  fStore.SetXYZ(x,y,z);
}
//____________________________________________________________________
void
Geometry::SetStorageBoxThickness(Double_t thick)  
{ 
  fStoreThick = thick; 
}

//____________________________________________________________________
void
Geometry::SetStorageBoxOffset(Double_t off)
{
  fZStoreOffset = off;
}

//____________________________________________________________________
void
Geometry::SetZ(Double_t z1, Double_t z2, Double_t z3, 
	       Double_t z4, Double_t z5)  
{
  fZ[0] = z1; 
  fZ[1] = z2; 
  fZ[2] = z3; 
  fZ[3] = z4; 
  fZ[4] = z5; 
}
//____________________________________________________________________
void
Geometry::SetZ(Double_t* zs)      
{ 
  for (size_t i=0; i<5; i++) fZ[i]  =zs[i]; 
}
//____________________________________________________________________
void
Geometry::SetPhi(Double_t* phis)  
{ 
  for (size_t i=0; i<5; i++) fPhi[i]=phis[i]; 
}
//____________________________________________________________________
void
Geometry::SetPhi(Double_t p1, Double_t p2, Double_t p3, 
		 Double_t p4, Double_t p5)  
{
  fPhi[0] = p1; 
  fPhi[1] = p2; 
  fPhi[2] = p3; 
  fPhi[3] = p4; 
  fPhi[4] = p5; 
}
//____________________________________________________________________
void
Geometry::SetZPhi(Int_t i, Double_t z, Double_t phi)  
{
  if (i < 0 || i > 4) return;
  fZ[i]   = z;
  fPhi[i] = phi;
}

//____________________________________________________________________
//
// EOF
//

  
