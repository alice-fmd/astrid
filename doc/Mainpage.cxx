//
// $Id: Mainpage.cxx,v 1.2 2006-06-20 23:59:43 hehi Exp $
//
//   ROOT generic simulation framework
//   Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//   This library is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public License 
//   as published by the Free Software Foundation; either version 2 of 
//   the License, or (at your option) any later version.  
//
//   This library is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details. 
//
//   You should have received a copy of the GNU Lesser General Public
//   License along with this library; if not, write to the Free
//   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
//   MA 02111-1307  USA  
//
//
/** @file   
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Title page documentation. */
/** @mainpage ALICE FMD test setup at ASTRID
    
    @section fmdtest_intro Introduction. 

    The package is based on my generic <a
    href="http://cern.ch/cholm/root/#framework">framework</a> and my
    generic <a href="http://cern.ch/cholm/root/#simulation">simulation
    framework</a>.  Both of these (and by extension <a
    href="http://root.cern.ch">ROOT</a>) need to be installed.
    Further, you also need a simulation back-end installed e.g., 
    <a href="http://root.cern.ch/root/vmc/VirtualMC.html">TGeant3</a>.

    To build, do 
    @verbatim
    ./configure <options> 
    make 
    @endverbatim 

    To use it, do 
    @verbatim
    root astrid/Config.C 
    @endverbatim 

    This reads in the configuration, and fires up a TBrowser.  To make
    one event, right-click the @c detector entry, and select @c
    Trigger. The geometry will be build (and possibly aligned), the
    back-end MC initialised, and one event propagated through the
    geometry.  The event processing will end with a event display
    pop-ing up, showing the tracks and the hits.  Use the buttons and
    slider to draw the event.  When done, press the @c Continue' button.
    You can now make more triggers as outlined above.  Batch
    processing is possible by doing
    @code
    root -b astrid/Config.C 
    Root> Simulation::Main* main = Simulation::Main::Instance();
    Root> main->Loop(nEvents);
    @endcode 

    The hits of the simulation is stored in a @c TClonesArray in @c
    TTree. The hits are objects of the class @c Hit.  The tracks
    marked for storage is also stored in the tree.  They are in a @c
    TClonesArray of @c TParticle.

    To use another generator, edit the first argument of 
    @code 
    TGenerator* eg = MakeGenerator(kNormal, 2000);
    @endcode 
    Any `TGenerator' derived class can be used, as long as the member
    function @c ImportParticles is defined to actually generate the
    particles - a noticeable exception to this is @c TPythia6. 
*/

#error Not for compilation
//
// EOF
//
